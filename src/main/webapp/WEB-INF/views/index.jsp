<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Real Estate Transactions</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body ng-app="myApp" class="ng-cloak">
<div class="generic-container" ng-controller="TransactionController as ctrl">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of Transactions </span></div>
        <div class="tablecontainer">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Street</th>
                    <th>City</th>
                    <th>Zip</th>
                    <th>State</th>
                    <th>Beds</th>
                    <th>Baths</th>
                    <th>Total Area</th>
                    <th>Residence Type</th>
                    <th>Sale Date</th>
                    <th>Price</th>
                    <th>Lattitude</th>
                    <th>Longitude</th>
                    <th width="20%"></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="u in ctrl.transactions">
                    <td><span ng-bind="u.street"></span></td>
                    <td><span ng-bind="u.city"></span></td>
                    <td><span ng-bind="u.zip"></span></td>
                    <td><span ng-bind="u.state"></span></td>
                    <td><span ng-bind="u.beds"></span></td>
                    <td><span ng-bind="u.baths"></span></td>
                    <td><span ng-bind="u.totalArea"></span></td>
                    <td><span ng-bind="u.residenceType"></span></td>
                    <td><span ng-bind="u.saleDate"></span></td>
                    <td><span ng-bind="u.price"></span></td>
                    <td><span ng-bind="u.latitude"></span></td>
                    <td><span ng-bind="u.longitude"></span></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="<c:url value='/static/js/app.js' />"></script>
<script src="<c:url value='/static/js/service/transaction_service.js' />"></script>
<script src="<c:url value='/static/js/controller/transaction_controller.js' />"></script>
</body>
</html>