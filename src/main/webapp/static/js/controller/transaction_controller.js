'use strict';

angular.module('myApp').controller('TransactionController', ['$scope', 'TransactionService', function($scope, TransactionService) {
    var self = this;
    self.transaction={street:'',city:'',zip:'',state:'',beds:0,baths:0,totalArea:0,residenceType:'',saleDate:'',price:0,latitude:0,longitude:0};
    self.transactions=[];

    fetchAllTransactions();

    function fetchAllTransactions(){
        TransactionService.fetchAllTransactions()
            .then(
            function(d) {
                self.transactions = d;
            },
            function(errResponse){
                console.error('Error while fetching Transactions');
            }
        );
    }

}]);
