'use strict';

angular.module('myApp').factory('TransactionService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://127.0.0.1:8081/proficiency-test/getList';

    var factory = {
        fetchAllTransactions: fetchAllTransactions
    };

    return factory;

    function fetchAllTransactions() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Transactions');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

}]);
